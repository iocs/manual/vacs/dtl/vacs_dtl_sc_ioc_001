# IOC for cross-DTL vacuum gauge controllers and gauges

## Used modules

*   [vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)


## Controlled devices

*   DTL-020:Vac-VEG-20001
    *   DTL-030:Vac-VGP-00021
    *   DTL-020:Vac-VGC-20000
    *   DTL-030:Vac-VGC-20000
*   DTL-040:Vac-VEG-20001
    *   DTL-010:Vac-VGC-20000
    *   DTL-040:Vac-VGC-20000
    *   DTL-050:Vac-VGC-20000
